package aufgaben;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CreateJSON {
	
	static JSONArray rows_1 = new JSONArray();
	static JSONArray rows_2 = new JSONArray();

	public static void main(String[] args) throws JSONException, IOException {
		
		try {
			Class.forName("org.postgresql.Driver");
		} catch(ClassNotFoundException e) {
			e.printStackTrace();
		}

		String database = "Election";
		String username = "postgres";
		String password = "pandufla";
		
		Connection con = null;
		Statement s = null;
		try {
			con = DriverManager.getConnection("jdbc:postgresql:" + database,
												username, password);
			s = con.createStatement();
		} catch(SQLException e) {
			e.printStackTrace();
			System.exit(0);
		}
		
		ResultSet rs = null;
		String query = "SELECT hashtag_name, text_id, time FROM hashtag ORDER BY time ASC";
		
		try {
			rs = s.executeQuery(query);
			
			while(rs.next()) {
			   
			   	
			   JSONObject row_time = new JSONObject();
			   JSONObject row_textid = new JSONObject();
				
			   row_time.put("name", rs.getString(1));
			   row_time.put("time", rs.getString(3).substring(0, 10));
			   rows_1.put(row_time);
			   
			   
			   row_textid.put("id", rs.getString(1));
			   row_textid.put("label", rs.getString(2));
			   rows_2.put(row_textid);
			}
			/*store the hashtags and the time-stamp in json file, which we will later use to visualize the hashtags use over the time with sigma js*/
			FileWriter file1 = new FileWriter("C:\\Users\\Matea\\Documents\\Latex files\\Datenbanksysteme\\hashtags_time.json");
			file1.write(rows_1.toString());		
			file1.close();
			
			/*store the hashtags and the text_id in json file, which we will later use to visualize the hashtag_netzwerk with sigma js*/
			FileWriter file2 = new FileWriter("C:\\Users\\Matea\\Documents\\Latex files\\Datenbanksysteme\\hashtags.json");
			file2.write(rows_2.toString());		
			file2.close();
			
		} catch(SQLException e) {
			e.printStackTrace();
		}

	}

}
